﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretWeapon : MonoBehaviour {
    public string m_name;//part_name
    public int m_weight;//weapon weight => correlate to pivot's weight limit
	
	void Start () {
        if (m_weight == 0)
            Destroy(gameObject);
	}

    public void Attack(Transform target)
    {
        if (gameObject.GetComponent<Rifle>() != null)
            gameObject.GetComponent<Rifle>().Aim(target);
    }
}
