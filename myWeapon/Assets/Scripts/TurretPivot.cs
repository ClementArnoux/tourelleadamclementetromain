﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretPivot : MonoBehaviour {

    public int m_size;//vertical pivot size
    public int m_weightLimit;//weapon's weight limit
    public string m_name;//part name

	void Start () {
        if (m_size == 0 || m_weightLimit == 0)
            Destroy(gameObject);
	}
	
}
