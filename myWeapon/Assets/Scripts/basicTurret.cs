﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basicTurret : MonoBehaviour {
    private ObjectHealth m_health;
    private GameObject m_weapon;
    private GameObject m_pivot;
    private GameObject m_base;

    public string m_baseName;
    public string m_pivotName;
    public string m_weaponName;
    public string m_name;

    private bool m_working = true;
    //must handle weapon weight on pivot weight limit and base size with turret slot size
	void Start () {
        var base_pos = transform.position;//put on the ground
        var pivot_pos = base_pos;

        m_health = GetComponent<ObjectHealth>();
        if(m_name == "")
        {
            m_name = "BasicTurret";
        }
        if(m_baseName == "" || m_pivotName == "" ||m_weaponName == "")
            m_working = false;
        if(m_working)
        {
            m_base = (GameObject)Instantiate(Resources.Load(m_baseName));
            m_pivot = (GameObject)Instantiate(Resources.Load(m_pivotName));
            m_weapon = (GameObject)Instantiate(Resources.Load(m_weaponName));

            m_base.GetComponent<Transform>().position = new Vector3(base_pos.x, base_pos.y + m_base.GetComponent<Renderer>().bounds.size.y/2, base_pos.z);
            base_pos = m_base.GetComponent<Transform>().position;
            m_pivot.GetComponent<Transform>().position = new Vector3(base_pos.x, base_pos.y + m_pivot.GetComponent<Renderer>().bounds.size.y / 2, base_pos.z);
            pivot_pos = m_pivot.GetComponent<Transform>().position;
            if(m_pivot.GetComponent<Renderer>() != null)
                m_weapon.GetComponent<Transform>().position = new Vector3(base_pos.x, pivot_pos.y + m_pivot.GetComponent<Renderer>().bounds.size.y / 2, base_pos.z);
            else
                m_weapon.GetComponent<Transform>().position = new Vector3(base_pos.x, pivot_pos.y + m_pivot.GetComponent<Collider>().bounds.size.y / 2, base_pos.z);

            if (m_base == null || m_pivot == null) //if import failed
                m_working = false;
            if (m_base.GetComponent<TurretBase>() == null || m_pivot.GetComponent<TurretPivot>() == null) //if wrong object type for current slot
                m_working = false;
        }
        if(!m_working)
            Destroy(gameObject);
    }

    public void AttackThisTarget(Transform target)
    {
        if (m_weapon.GetComponent<TurretWeapon>() != null)
            m_weapon.GetComponent<TurretWeapon>().Attack(target);
    }
}
