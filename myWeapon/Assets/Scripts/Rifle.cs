﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : MonoBehaviour {
    public bool activate;
    public GameObject   m_bullet;
    public Transform    m_bulletSpawn;
    public int          m_bulletSpeed;
    public float        rotatespeed;
    public float        rate;
    bool                aiming;
    bool                shooting;
    float               lastStep;
    Transform           Target;
    Vector3             direction;
    Quaternion          rotationAngle;
    Vector3             old;


    void Start()
    {
        Debug.Log("This is a test");
        lastStep = Time.time;
        aiming = false;
    }

    public void Aim(Transform Targetparam)
    {
        Target = Targetparam;
        old = Target.position;
        aiming = true;
        shooting = true;
    }

    void FixedUpdate ()
    { 
        if (!activate)
            return;
        if (aiming == true)
        {

            direction = Target.position * 2 - old - transform.position ;
            old = Target.position;
            rotationAngle = Quaternion.LookRotation(Target.position - transform.position);
            rotationAngle = Quaternion.Euler(rotationAngle.eulerAngles.x + 90, rotationAngle.eulerAngles.y, rotationAngle.eulerAngles.z);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotationAngle, Time.deltaTime * rotatespeed);
        }

        if (shooting == true)
          {
            if (Time.time - lastStep > rate)
               {
                    lastStep = Time.time;
                    Fire();
                }
            }
     }

    public void Shoot()
    {
        shooting = true;
    }

    public void Stop()
    {
        shooting = false;
        aiming = false;
    }

    private void Fire()
    {
        var ammo = Instantiate(m_bullet, m_bulletSpawn.position, m_bulletSpawn.rotation);
        ammo.GetComponent<Rigidbody>().AddForce(ammo.transform.up * m_bulletSpeed);
    }
}
