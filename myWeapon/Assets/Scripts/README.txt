### BEGIN README ###

Script TowerGroup.cs

� attacher � un GameObject vide, repr�sentant le groupe de tours. L'emplacement de l'objet dans la sc�ne n'a pas d'importance. Chaque tour appartenant au groupe peut communiquer avec les autres.

Remplissez le tableau Towers avec les GameObjects repr�sentant les tours du groupe.


---

Script Tags.cs

� attacher � n'importe quel GameObject. Ce script n'a aucune influence sur les objets de la sc�ne mais permet d'utiliser un syst�me de tags plus pouss� pour la raison suivante : le syst�me de tags d'Unity permet, � ma connaissance, d'utiliser un seul tag � la fois. Avec celui-ci, il suffit de cocher des cases.

Pour l'instant, une tour de garde d�tecte des objets qui portent le tag Player de ce script. On pourra s'en servir � d'autres fins si n�cessaire.


---

Script GuardTower.cs

� attacher � un GameObject repr�sentant une tour. Ce script permet � une tour de d�tecter des GameObjects ennemis dans un certain rayon. Si c'est le cas, elle envoie l'instruction "Attaquez � cet endroit !" au groupe de tours auquel elle appartient. S'il n'y a plus d'ennemi � port�e, la tour n'envoie plus d'instructions.

Le champ Group contient une r�f�rence vers le GameObject portant le script TowerGroup.cs. Les messages de cette tour seront envoy�s au groupe via cette r�f�rence.

Le champ Range repr�sente la port�e de d�tection de la tour.


---

Script AttackTower.cs

� attacher � un GameObject repr�sentant une tour. Ce script permet � une tour d'attaquer � un endroit selon les instructions re�ues de la part de son groupe de tours.

Le champ Group contient une r�f�rence vers le GameObject portant le script TowerGroup.cs.

Le champ Head contient une r�f�rence vers le GameObject repr�sentant l'arme de la tour. Le GameObject s'oriente vers la cible en cas d'attaque.

Pour l'instant, la tour se contente de s'orienter vers une cible. La fonction AttackThisTarget() de ce script pourra �tre compl�t�e afin de v�ritablement attaquer.

---

Remarques :

- Il est possible d'attacher les scripts GuardTower.cs et AttackTower.cs � une m�me tour.
- Pour l'instant, une tour de garde d�tecte les ennemis s'ils sont simplement assez pr�s, m�me s'ils sont cach�s. La gestion des obstacles ainsi que celle des cibles multiples viendront apr�s.

### END README ###