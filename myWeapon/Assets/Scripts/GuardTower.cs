﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GuardTower : MonoBehaviour
{
    public TowerGroup Group;
    
    public float Range = 10.0f;
    

    void Start()
    {

    }
    
    void Update()
    {
        Tags[] list = FindObjectsOfType<Tags>();

        foreach (Tags e in list)
        {
            if (e.Player)
            {
                if (Vector3.Distance(transform.position, e.transform.position) <= Range)
                    Group.SendPositionToAttackTowers(e.transform);
            }
        }
    }
}
